/**
 * @ngdoc overview
 * @name transcend.lrs-grid-editor
 * @description
 # LRS Grid Editor Module
 The LRS Grid Editor Module provides a component which edits LRS data in an ESRI map instance in a custom way.

 ## Functionality
 The difference between the components in this module and the standard grid editing tools found in RCE and AngularUI is
 that instead of merely editing data records directly, the components define an editing process which allows a history
 to be kept.

 ## Installation
 The easiest way to use this, and other Transcend libraries, is to use [Bower.js](http://bower.io/). Run the following
 commands to install this package:

 ```
 bower install https://bitbucket.org/transcend/bower-lrs-grid-editor.git
 ```

 Other options:

 * Download the code at [http://code.tsstools.com/bower-lrs-grid-editor/get/master.zip](http://code.tsstools.com/bower-lrs/get/master.zip)
 * View the repository at [http://code.tsstools.com/bower-lrs-grid-editor](http://code.tsstools.com/bower-lrs-grid-editor)
sdd
 When you have pulled down the code and included the files (and dependency files), simply add the module as a
 dependency to your application:

 ```
 angular.module('myModule', ['transcend.lrs-grid-editor']);
 ```

 ## To Do
 - Complete 100% unit test coverage.
 */
/**
 * @ngdoc object
 * @name transcend.lrs-data-grid.lrsGridEditorConfig
 *
 * @description
 * Provides a single configuration object that can be used to change the behaviour, options, urls, etc for any
 * components in the {@link transcend.lrs LRS Module}. The "lrsConfig" object is a
 * {@link http://docs.angularjs.org/api/auto/object/$provide#value module value} and can therefore be
 * overridden/configured by:
 *
 <pre>
 // Override the value of the 'esriConfig' object.
 angular.module('myApp').value('lrsConfig', {
     lrsProfile: {
       url: 'http://some-other-service/lrs'
     }
   });
 </pre>
 **/
/**
 * @ngdoc directive
 * @name transcend.lrs-data-grid.lrsDataGrid
 *
 * @description
 * A wrapper for {@link transcend.dynamic-grid.dynamicGrid}, specifically for visualizing data return from esri
 * MapServer calls.
 *
 * In the future, this directive will support the retire methodology used in RCE for keeping a history.
 *
 * @restrict E
 * @element ANY
 *
 * @param {object} esriData The data to visualize -- must be the return from a MapServer query
 * @param {boolean} showControls Whether or not to show the map controls
 * @param {object} [esriMap] The ArcGis JS Api map from which the data was pulled.
 *
 * @requires lrsDataGridConfig
 * @requires transformLrsData
 * @requires $window
 */
/**
 * @ngdoc filter
 * @name transcend.lrs-data-grit.filter:resolvedFilter
 *
 * @description
 * The 'resolvedFilter'
 */
/**
 * @ngdoc service
 * @name transcend.lrs-data-grid.directive:transformLrsData
 *
 * @description
 * The 'transformLrsData' factory provides methods for transforming LRS data into the dynamic-grid format.
 *
 * @requires transcend.core.$string
 * @requires transformLrsDataConfig
 *
 * @param {object} esriData the raw esri Data
 * @param {boolean} injections flag to check to see if additional attributes are applied
 **/
